from pygame import *
from random import randint
from time import time as timer


# базовый класс для спрайтов
class GameSprite(sprite.Sprite):
    """
    image_file - имя файла с картинкой для спрайта
    x - координата x спрайта
    y - координата y спрайта
    speed - скорость спрайта
    size_x - размер спрайта по горизонтали
    size_y - размер спрайта по вертикали
    """

    # конструктор
    def __init__(self, image_file, x, y, speed, size_x, size_y):
        super().__init__()  # конструктор суперкласса
        self.image = transform.scale(
            image.load(image_file), (size_x, size_y)
        )  # создание внешнего вида спрайта - картинки
        self.speed = speed  # скорость
        self.rect = (
            self.image.get_rect()
        )  # прозрачная подложка спрайта - физическая модель
        self.rect.x = x
        self.rect.y = y

    # метод для отрисовки спрайта
    def reset(self):
        # отобразить картинку спрайта в тех же координатах, что и его физическая модель
        window.blit(self.image, (self.rect.x, self.rect.y))


# класс для игрока (наследник от основного класса)
class Player(GameSprite):
    # метод для управления игрока стрелками клавиатуры
    def update(self):
        # получаем словарь состояний клавиш
        keys = key.get_pressed()

        # если нажата клавиша влево и физическая модель не ушла за левую границу игры
        if keys[K_LEFT] and self.rect.x > 5:
            # двигаем влево
            self.rect.x -= self.speed

        # если нажата клавиша вправо и физическая модель не ушла за правую границу игры
        if keys[K_RIGHT] and self.rect.x < width - 70:
            # двигаем вправо
            self.rect.x += self.speed

    # метод для стрельбы пулями
    def fire(self):
        bullet = Bullet(img_bullet, self.rect.centerx, self.rect.top, 15, 20, 15)
        bullets.add(bullet)


# класс для врага (наследник от основного класса)
class Enemy(GameSprite):
    # метод для движения врага
    def update(self):
        global lost  # используем глобальную переменную
        self.rect.y += self.speed  # двигаем врага вниз

        # если координата Y врага вышла за нижнюю границу экрана
        if self.rect.y > height:
            # устанавливаем случайную координату по X
            self.rect.x = randint(80, width - 80)
            # устанавливаем координату по Y (немного выше верхней границы)
            self.rect.y = -50
            # к количеству пропущенных прибавляем 1
            lost += 1


# класс для препятствия (наследник от основного класса)
class Barrier(GameSprite):
    # метод для движения препятствия
    def update(self):
        self.rect.y += self.speed  # двигаем препятствия вниз

        # если координата Y препятствия вышла за нижнюю границу экрана
        if self.rect.y > height:
            # устанавливаем случайную координату по X
            self.rect.x = randint(80, width - 80)
            # устанавливаем координату по Y (немного выше верхней границы)
            self.rect.y = -50


# класс для пуль
class Bullet(GameSprite):
    # метод для движения пули
    def update(self):
        self.rect.y -= self.speed
        if self.rect.y < 0:
            self.kill()


""" 
Основная часть программы
Создание окна, задание параметров,
создание спрайтов
"""

# размеры окна
width = 700
height = 500

# создание окна
window = display.set_mode((width, height))
display.set_caption("Space Invaders")

# названия файлов
img_back = "galaxy.jpg"  # фон игры
img_hero = "rocket.png"  # игрок
img_enemy = "ufo.png"  # враг
img_barrier = "asteroid.png"  # препятствие
img_bullet = "bullet.png"  # пуля

# открываем фон сцены
background = transform.scale(image.load(img_back), (width, height))

# подключаем музыку
mixer.init()
mixer.music.load("space.ogg")  # фоновая музыка
mixer.music.play()
fire_sound = mixer.Sound("fire.ogg")  # звук выстрела

# переменная окончания игры
finish = False  # когда True, то спрайты перестают работать
# переменная завершения программы
game = True  # завершается при нажатии кнопки закрыть окно
# переменная перезарядки
reload_bullets = False  # если True - происходит перезарядка

# счетчики
score = 0  # счетчик сбитых
lost = 0  # счетчик пропущенных
max_lost = 10  # максимум пропущенных
max_score = 20  # количество очков для победы
life = 3  # количество жизней
max_bullets = 10  # максимум пуль в обойме
num_bullet = 0  # количество пуль

# шрифт
font.init()
font1 = font.SysFont("Arial", 36)
font2 = font.SysFont("Arial", 80)
win = font2.render("YOU WIN!", True, (255, 255, 255))
lose = font2.render("YOU LOSE!", True, (180, 0, 0))

# внутриигровые часы и ФПС
clock = time.Clock()
FPS = 60

""" Создание спрайтов """
# спрайт игрока (корабль)
ship = Player(img_hero, 5, height - 100, 10, 80, 100)

# создание группы врагов (НЛО)
monsters = sprite.Group()
# 5 врагов
for i in range(5):
    # создаем врага с картинкой img_enemy
    # со случайной координатой X и координатой Y = -40
    # со случайной скоростью от 1 до 4
    # и размером 80*50
    monster = Enemy(img_enemy, randint(0, width - 80), -40, randint(1, 4), 80, 50)
    # добавляем его в группу
    monsters.add(monster)

# создание группы препятствий
asteroids = sprite.Group()
for i in range(3):
    # создаем препятствие с картинкой img_enemy
    # со случайной координатой X и координатой Y = -40
    # со случайной скоростью от 1 до 4
    # и размером 50*50
    asteroid = Barrier(img_barrier, randint(0, width - 50), -40, randint(1, 4), 50, 50)
    # добавляем его в группу
    asteroids.add(asteroid)

# создание группы пуль (изначально пустая)
bullets = sprite.Group()

""" Игровой цикл"""

# пока игровой цикл (как программы) работает
while game:
    # обработка нажатия кнопки Закрыть окно
    for e in event.get():
        if e.type == QUIT:
            game = False  # завершение игрового цикла (как программы)
        # если нажата
        elif e.type == KEYDOWN:
            # клавиша пробел
            if e.key == K_SPACE:
                # проверка возможности выстрела
                # пуль меньше max_bullets и нет перезарядки
                if num_bullet < max_bullets and reload_bullets == False:
                    fire_sound.play()  # звук выстрела
                    ship.fire()  # метод выстрела
                    num_bullet += 1  # увеличиваем количество пуль
                if num_bullet >= max_bullets and reload_bullets == False:
                    last_time = timer()
                    reload_bullets = True

    if finish != True:
        window.blit(background, (0, 0))  # добавление фона на кадр

        # создаем надпись для счета
        text = font1.render("Счет: " + str(score), True, (255, 255, 255))
        # и добавляем её на экран
        window.blit(text, (10, 20))

        # создаем надпись для пропущенных врагов
        text_lose = font1.render("Пропущено: " + str(lost), True, (255, 255, 255))
        # и добавляем её на экран
        window.blit(text_lose, (10, 50))

        ship.update()  # передвижение игрока
        monsters.update()  # передвижение врагов
        bullets.update()  # передвижение пуль
        asteroids.update()  # передвижение препятствий

        ship.reset()  # добавление игрока на кадр
        monsters.draw(window)  # добавление монстров на кадр
        bullets.draw(window)  # добавление пуль на кадр
        asteroids.draw(window)  # добавление препятствий на кадр

        # если запущена перезардка
        if reload_bullets:
            # фиксируем время начала перезарядки
            now_time = timer()
            # если НЕ прошло 3 секунды
            if now_time - last_time < 3:
                # создаем надпись для перезарядки
                reload_text = font2.render("Wait, reload...", True, (150, 0, 0))
                # выводим на экран
                window.blit(reload_text, (200, 400))
            # если прошло 3 секунды
            else:
                # обнуляем количество пуль
                num_bullet = 0
                # завершаем перезарядку
                reload_bullets = False

        # если игрок коснулся врага или препятствие
        if sprite.spritecollide(ship, monsters, False) or sprite.spritecollide(
            ship, asteroids, False
        ):
            # удаляем с экрана врага или препятствие
            sprite.spritecollide(ship, monsters, True)
            sprite.spritecollide(ship, asteroids, True)
            # уменьшаем количество жизней
            life -= 1

        # столкновение пуль и врагов
        collides = sprite.groupcollide(monsters, bullets, True, True)
        # перебираем все столкновения
        for c in collides:
            score += 1  # добавляем 1 очко
            # создаем нового врага
            monster = Enemy(
                img_enemy, randint(0, width - 80), -40, randint(1, 4), 80, 50
            )
            monsters.add(monster)  # и добавляем его в группу

        # проигрыш - проверка столкновения игрока с одним врагом
        if life == 0 or lost >= max_lost:
            finish = True  # завершаем игру
            window.blit(lose, (200, 200))  # выводим надпись о проигрыше

        # выигрыш - набрали больше max_score очков
        if score >= max_score:
            finish = True  # завершаем игру
            window.blit(win, (200, 200))  # выводим надпись о выигрыше

        # # Окрас счетчика жизней
        # если жизни 3
        if life == 3:
            life_color = (0, 150, 0)  # зеленый цвет
        # если жизни 2
        elif life == 2:
            life_color = (150, 150, 0)  # желтый цвет
        # если жизнь 1
        elif life == 1:
            life_color = (150, 0, 0)  # красный цвет

        # создаем надпись с количеством жизней
        life_text = font2.render(str(life), True, life_color)
        # выводим её на экран
        window.blit(life_text, (650, 10))

    # перезапуск игры
    else:
        # обнуляем все переменные до исходных значейний
        finish = False
        score = 0
        lost = 0
        num_bullet = 0
        life = 3
        reload_bullets = False

        # удаляем все спрайты с экрана (кроме игрока)
        for b in bullets:
            b.kill()
        for m in monsters:
            m.kill()
        for a in asteroids:
            a.kill()

        # делаем паузу
        time.delay(3000)

        # создаем 5 врагов заново
        # и добавляем их в группу
        for i in range(5):
            monster = Enemy(
                img_enemy, randint(0, width - 80), -40, randint(1, 4), 80, 50
            )
            monsters.add(monster)

        # создаем 3 препятствия заново
        # и добавляем их в группу
        for i in range(3):
            asteroid = Barrier(
                img_barrier, randint(0, width - 50), -40, randint(1, 4), 50, 50
            )
            asteroids.add(asteroid)

    # обновляем все содержимое на экране
    display.update()
    clock.tick(FPS)
